import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  submitted: boolean = false;
  constructor(private fb: FormBuilder) { }
  // private profile: FormBuilder
  private profileForm: FormGroup;
  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, this.emailDomainValidator]],
      address: this.fb.group({
        street: [''],
        city: [''],
        state: [''],
        zip: ['']
      }),
    });
    // this.profileForm.controls.firstName.setValue("usman");
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    else {
      console.warn(this.profileForm.value);
    }
  }
  get form() {
    return this.profileForm.controls;
  }
  emailDomainValidator(control: AbstractControl): { [key: string]: any } | null {
    const email: string = control.value;
    const domain = email.substring(email.lastIndexOf('@') + 1);
    if (email === "" || domain === 'gmail.com') {
      return null;
    }
    else {
      return { "domain": true };
    }
  }
}